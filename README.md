#Disclaimer
OK guys this a prototype, the code is pure spaghetti so lets just use it and if i've got time i'll refactor it
#Requirements
* you need to have Docker (in windows install **docker for windows**) installed
* you need to have python3 installed
* you need to have visual c++ 2017?8 installed alongside windows sdk or pip won't compile some libraries
#Usage
You need to install all packages using pip so :  
`pip install -r requirements.txt`  

then you need to run mysql container using Docker:  
‍‍‍`docker-compose up -d`

then check if mysql is running using either :  
`docker-compose ps`  
or trying to connect to `localhost:7979`  
you can find mysql user, password in `docker-compose.yml`

after you've done you need to run migration so tables get created:  
`python main.py migrate`  
this output nothing if everything goes as planned so you can see there are 5 tables in MySQL  

#Commands
Add tag(this is categories so each user maybe assigned to a category):  
`python main.py add_tag {TAGNAME}`  
sample:
`python main.py add_tag test`  

Add user:  
`python main.py add_user {USERNAME} {TAGNAME}`  
sample:  
`python main.py add_user farshad test`  

Update all users following and followers count:  
> this command may take a lot time to complete
  
`python main.py update_user`  

Get tweets:  
`python main.py read_tweets {SEARCH STRING}`  
sample  
`python main.py read_tweets #کدشعر`  

Get report in CSV format:  
`python main.py get_report {SEARCH STRING}`  
sample  
`python main.py get_report #کدشعر`  
this will result in a file named **export.csv**, remember to delete this file or move to someplace else before running this command again


