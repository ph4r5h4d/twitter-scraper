import csv
import os
import time
import twint
import fire

from models.tag import Tag
from models.user import User
from models.tweet import Tweet
from models.hashtag import Hashtag
from models.user_hashtag_stats import UserHashtagStats
from helpers.file_helper import fileHelper
from peewee import DoesNotExist


class main(object):

    def migrate(self):
        Tag.create_table()
        User.create_table()
        Hashtag.create_table()
        Tweet.create_table()
        UserHashtagStats.create_table()

    def add_tag(self, tag_name):
        tag = Tag(name=tag_name)
        try:
            if tag.save() == 1:
                print('tag:' + tag_name + ' created with id:' + str(tag.id))
            else:
                print('failed to add tags , check database connection!')
        except:
            print('failed to add tags , probably it exists or an error occured during database connection')

    def getOrCreateHashtag(self, hashtag):
        try:
            ht = Hashtag.get(Hashtag.name == hashtag)
            return ht.id
        except DoesNotExist:
            try:
                ht = Hashtag(name=hashtag)
                if ht.save() == 1:
                    return ht.id
                else:
                    raise Exception('unable to save hashtag into database')
            except Exception as e:
                raise e

    def add_user(self, tag_name, username):
        global tag
        try:
            tag = Tag.get(Tag.name == tag_name)
        except:
            print('tag does not exists')

        user = User(username=username, tag=tag.id)
        try:
            if user.save() == 1:
                print('user added to the database')
            else:
                print('failed to add user to database')
        except:
            print('it is either a duplicate user or a problem in database connection')

    def update_user(self):
        twitter = twint.Config()
        for user in User.select():
            twitter.Username = user.username
            twitter.Output = user.username + '.txt'
            twint.run.Followers(twitter)
            num_followers = fileHelper.file_len(user.username + '.txt')
            user.followers = num_followers
            user.save()
            os.remove(user.username + '.txt')
            twint.run.Following(twitter)
            num_following = fileHelper.file_len(user.username + '.txt')
            user.following = num_following
            user.save()
            os.remove(user.username + '.txt')
            print('user ' + user.username + ' updated with ' + str(num_followers) + ' followers and ' +
                  str(num_following) + ' following')
            time.sleep(5)

    def add_to_user_hashtag_counter(self, user_id, hashtag_id):
        try:
            stats = UserHashtagStats.get(user_id=user_id, hashtag_id=hashtag_id)
            stats.count += 1
        except DoesNotExist:
            stats = UserHashtagStats(user_id=user_id, hashtag_id=hashtag_id, count=1)

        if stats.save() == 1:
            return True
        else:
            raise Exception('failed to update stats')

    def read_tweets(self, hashtag):
        try:
            hashtag_id = self.getOrCreateHashtag(hashtag)
        except:
            exit(1)

        c = twint.Config()
        c.Search = hashtag
        c.Store_csv = True
        c.Custom = ["id", "user_id", "username", "retweets", "likes", "time", "date", "tweet"]
        c.Output = "twitter.csv"
        c.Limit = 5
        twint.run.Search(c)
        with open('twitter.csv', newline='', encoding='utf-8') as csvfile:
            data = list(csv.reader(csvfile))

        del data[0]
        count = 0
        for tweet_data in data:
            t = Tweet(tweet_id=tweet_data[0], tweeter_user_id=tweet_data[1], user_name=tweet_data[2],
                      retweets=tweet_data[3], likes=tweet_data[4], tweet_time=tweet_data[5],
                      tweet_date=tweet_data[6], tweet=tweet_data[7], hashtag_id=hashtag_id)
            try:
                user = User.get_user_by_username(tweet_data[2])
            except:
                user = False

            if user:
                try:
                    self.add_to_user_hashtag_counter(user.id, hashtag_id)
                except:
                    pass

            try:
                t.save()
                count += 1

            except:
                # print('duplicate entry... continuing ...')
                continue

        os.remove('twitter.csv')
        print('Total ' + str(count) + ' tweets added to database ')

    def get_report(self, hashtag):
        try:
            hashtag_id = Hashtag.get(name=hashtag)
        except:
            print('hastag doesn\'t exist')
            exit(1)

        try:
            users_on_hashtag = UserHashtagStats.select().where(UserHashtagStats.hashtag_id == hashtag_id)
        except:
            print('nothing on this hashtag, make sure to collect tweets before running this method')
            exit(1)

        if os.path.isfile('export.csv'):
            try:
                os.remove('export.csv')
            except:
                print('oops export.csv exists and it is open by another app. we stop here...')
                exit(1)

        fh = open('export.csv', 'w', encoding='utf-8')
        fh.write("id,user_id,username,retweets,likes,time,date,tweet,fetch_date\n")
        for stats in users_on_hashtag:
            username = User.get(id=stats.user_id).username
            tweets = Tweet.select().where((Tweet.user_name == username) & (Tweet.hashtag_id == hashtag_id))
            for tweet in tweets:
                fh.write(
                    str(tweet.tweet_id) + "," + str(tweet.tweeter_user_id) + "," + tweet.user_name
                    + "," + str(tweet.retweets) + "," + str(tweet.likes) + "," + str(tweet.tweet_time)
                    + "," + str(tweet.tweet_date) + "," + tweet.tweet + ',' + tweet.created_at + '\n')

        fh.close()
        print('I think everything is exported')


if __name__ == '__main__':
    fire.Fire(main)
