import datetime
from peewee import *
from .basemodel import BaseModel


class Hashtag(BaseModel):
    name = CharField(unique=True,collation='utf8_general_ci')
    created_at = DateTimeField(default=datetime.datetime.now)

