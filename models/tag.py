import datetime
from peewee import *
from .basemodel import BaseModel


class Tag(BaseModel):
    name = CharField(unique=True)
    created_at = DateTimeField(default=datetime.datetime.now)

