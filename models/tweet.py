import datetime
from peewee import *
from .basemodel import BaseModel
from .hashtag import Hashtag


class Tweet(BaseModel):
    # user_id = ForeignKeyField(User, field='id', backref='tweet')
    tweet_id = BigIntegerField(unique=True)
    tweeter_user_id = BigIntegerField()
    user_name = CharField()
    retweets = IntegerField()
    likes = IntegerField()
    tweet_date = DateField()
    tweet_time = TimeField()
    hashtag_id = IntegerField(default=0)
    tweet = TextField(collation='utf8_general_ci')
    created_at = DateTimeField(default=datetime.datetime.now)
