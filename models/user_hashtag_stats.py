import datetime
from peewee import *
from .basemodel import BaseModel


class UserHashtagStats(BaseModel):
    user_id = IntegerField(default=0)
    hashtag_id = IntegerField(default=0)
    count = IntegerField(default=0)
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        indexes = (
            (('user_id', 'hashtag_id'), True),
        )
