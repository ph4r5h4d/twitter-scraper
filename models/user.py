import datetime
from peewee import *
from .basemodel import BaseModel
from .tag import Tag


class User(BaseModel):
    username = CharField(unique='true')
    following = IntegerField(default=0)
    followers = IntegerField(default=0)
    tag = ForeignKeyField(Tag, backref='user')
    created_at = DateTimeField(default=datetime.datetime.now)

    @staticmethod
    def get_user_by_username(username):
        try:
            user = User.get(username = username)
            return user
        except:
            raise
